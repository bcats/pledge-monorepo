import React, { useState, useEffect } from "react";
// import SmartPledge from "../contracts/SmartPledge.json";
import SmartPledge from "../contracts/SmartPledgeRinkeby.json";
import truffleContract from "truffle-contract";
// Import blockchain connection utils
import getWeb3 from "../utils/getWeb3";
import * as IPFShashHelpers from "../utils/IPFShashHelpers";

function withContract(WrappedComponent) {
  return function SmartPledgeContractService(props) {
    let [contract, setContract] = useState(null);
    let [web3, setWeb3] = useState(null);
    let [account, setAccount] = useState(null);
    let [gasPrice, setGasPrice] = useState(null);
    const { bytes32FromIpfs, ipfsFromBytes32 } = IPFShashHelpers;

    useEffect(() => {
      async function getWeb3Stuff() {
        try {
          const web3 = await getWeb3();
          console.log("got web3", web3);
          const gasPrice = 42000000000;
          // Get the contract
          const Contract = truffleContract(SmartPledge);
          console.log("got contract", Contract);
          Contract.setProvider(web3.givenProvider);
          const contractInstance = await Contract.deployed();
          console.log("got contract instance", contractInstance);

          setContract(contractInstance);
          setWeb3(web3);
          setGasPrice(gasPrice);
        } catch (error) {
          // Catch any errors for any of the above operations.
          console.log(`Failed to load contract.`);
          console.log(error);
        }
      }

      getWeb3Stuff();
    }, []);

    useEffect(() => {
      async function getAccounts() {
        if (web3) {
          const accounts = await web3.eth.getAccounts();
          console.log("got accounts", accounts);
          setAccount(accounts[0]);
        } else {
          setAccount(null)
        }
      }
      getAccounts();
    }, [web3]);

    return (
      <WrappedComponent
        contract={contract}
        bytes32FromIpfs={bytes32FromIpfs}
        ipfsFromBytes32={ipfsFromBytes32}
        web3={web3}
        gasPrice={gasPrice}
        account={account}
        {...props}
      />
    );
  };
}

export default withContract;
