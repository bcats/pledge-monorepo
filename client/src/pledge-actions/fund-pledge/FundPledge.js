import React, { useState } from "react";

import { NavLink } from "react-router-dom";
import LoadingWithMessage from '../../utils/loaders/LoadingWithMessage';
import "./FundPledge.css";

// Import pledge components
import SimplePledge from "../../pledge-components/simple-pledge/SimplePledge";

import withPouchDB from "../../database/withPouchDB";
import withSinglePledgeFromURL from "../../database/withSinglePledgeFromURL";

function FundPledge(props) {
  const { pledge } = props;
  let [stripeLoading, setStripeLoading] = useState(false);

  const stripeCheckoutSubmit = sku => {
    setStripeLoading(true);
    const stripe = window.Stripe(`${process.env.REACT_APP_STRIPE_KEY}`, {
      betas: ["checkout_beta_4"]
    });
    stripe
      .redirectToCheckout({
        items: [{ sku: sku, quantity: 1 }],
        clientReferenceId: `${props.userId}/${pledge.pledgeId}/${
          pledge.ipfsHash
        }`,

        // Note that it is not guaranteed your customers will be redirected to this
        // URL *100%* of the time, it's possible that they could e.g. close the
        // tab between form submission and the redirect.
        // So we pass details in the clientReferenceId above for the webhook to handle the pledge
        successUrl: `${process.env.REACT_APP_MVP_URL}/pledge/${
          pledge.pledgeId
        }?paymentSuccess=true`,
        cancelUrl: `${process.env.REACT_APP_MVP_URL}/pledge/payment-cancel` // TODO implement route
      })
      .then(function(result) {
        if (result.error) {
          // If `redirectToCheckout` fails due to a browser or network
          // error, display the localized error message to your customer.
          var displayError = document.getElementById("error-message");
          displayError.textContent = result.error.message;
        }
      });
  };

  return (
    <div className="FundPledge">
      {pledge && (
        <SimplePledge pledgeData={pledge}>
          <hr className="uk-divider-small" />
          <h2>Fund this pledge</h2>
          {stripeLoading && (
            <LoadingWithMessage message="Connecting you to Stripe" />
          )}
          {!stripeLoading && (
            <>
              <p>
                If you want to see this pledge actually happen, up the incentive
                by funding it now:
              </p>
              <p className="uk-alert-success" data-uk-alert>
                During our testing phase you can use the test card details which
                are 4242 4242 4242 4242 with any postcode, valid to date and
                security code.
              </p>
              <p uk-margin>
                <button
                  className="uk-button uk-button-primary uk-button-large uk-margin-right"
                  onClick={() => stripeCheckoutSubmit("sku_EiuQxQ6Z3h3uVl")}
                >
                  Add $5
                </button>

                <button
                  className="uk-button uk-button-primary uk-button-large uk-margin-right"
                  onClick={() => stripeCheckoutSubmit("sku_EiuWU9bg9ZLDD4")}
                >
                  Add $10
                </button>

                <button
                  className="uk-button uk-button-primary uk-button-large uk-margin-right"
                  onClick={() => stripeCheckoutSubmit("sku_EiuWxHDGH0FpuS")}
                >
                  Add $25
                </button>
              </p>
            </>
          )}

          {/* {!web3 && (
            <LoadingWithMessage message="Loading Web3. Please enable metamask" />
          )} */}
        </SimplePledge>
      )}
    </div>
  );
}
export default withPouchDB(withSinglePledgeFromURL(FundPledge));
